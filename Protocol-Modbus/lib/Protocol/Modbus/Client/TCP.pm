package Protocol::Modbus::Client::TCP;
use strict;
use warnings;

use Carp;
use IO::Socket;
use Object::Tiny;
use Protocol::Modbus::Log qw(to_log);
use Protocol::Modbus::TCP qw(build_adu parse_adu);

#
# TCP backend constructor
#
sub new {
    my $class = shift;
    my $self = $class->SUPER::new(@_);

    my %args = @_;
    
    $self->{settings} = $args{settings};
    
    # Init transaction ID counter
    $self->{transact_cnt} = 0;

    return $self;
}

#
#
#
sub request {
    my $self = shift;

    # Build ADU
    my $req = build_adu($self->{transact_cnt}++, @_);
    
    # Setup socket
    my $socket = IO::Socket::INET->new(
        PeerAddr => $self->{settings}{address},
        PeerPort => $self->{settings}{port},
        Proto    => 'tcp',
        Timeout  => 1
    ) or die "Error: can't create socket: $@\n";
    
    # Send request
    print $socket $req;
    shutdown($socket, 1);
    to_log('request', $req);
    
    # Wait
    # sleep 3;
    
    my $resp;
    eval {
        local $SIG{ALRM} = sub { die "alarm\n" }; # NB: \n required
        alarm 5;
        $resp = <$socket>;
        alarm 0;
    };
    if ($@) {
        die unless $@ eq "alarm\n";   # propagate unexpected errors
        # timed out
        warn "Timeout";
        close($socket);
        return;
    }
    
   
    # shutdown($socket, 0);
    close($socket);
    to_log('response', $resp);
    
    # Parse received response
    my ($mbap_header, $pdu) = parse_adu($resp);
    #to_log('response', $mbap_header);
    #to_log('response', $pdu);
    
    # Return response
    return $pdu;
}

1;
