package Protocol::Modbus::Client::ReadInputRegisters;
require Exporter;

use strict;
use warnings;

use Carp;
use Protocol::Modbus qw(%FUNCTION_CODE build_pdu_frame);
use Protocol::Modbus::Log qw(to_log);

our @ISA       = qw(Exporter);
our @EXPORT    = qw();
our @EXPORT_OK = qw(read_input_registers);

#
# Modbus function "read input registers"
#
sub read_input_registers {
    my ($self, %arg) = @_;

    # Use address 0 by default for remote Unit ID
    $arg{server_address} ||= 0;

    # Build data frame
    my $data_frame = _build_data_frame(
        $arg{starting_address}, $arg{quantity_of_registers}
    );

    # Build PDU frame
    my $request = build_pdu_frame(
        $FUNCTION_CODE{read_input_registers}, $data_frame
    );

    # Send query with current connector and get response
    my $response = $self->{backend}->request($arg{server_address}, $request) ||
        return;

    # Return array of input registers
    _parse_response($response);
}

#-------------------------------------------------------------------------------
#
#                          Private subroutines
#
#-------------------------------------------------------------------------------

#
# Returns built data frame
#
sub _build_data_frame {
    my ($starting_address, $quantity_of_registers) = @_;
    pack("nn", $starting_address, $quantity_of_registers);
}

#
# Returns exception description by its code
#
sub _get_modbus_exception {
    # my $exception_code = shift;

    # # Check exception code
    # unless (exists $EXCEPTION_CODE{$exception_code}) {
        # return sprintf("Unknown exception code (%d)\n", $exception_code);
    # }

    # # Return exception description
    # $EXCEPTION_CODE{$exception_code};
}

#
# Parses response and returns
#
sub _parse_response {
    my $response = shift;

    # Unpack received response
    my @response_bytes = unpack('CCn*', $response);
    my $function_code = $response_bytes[0];

    # # Check for exception
    # if ($function_code & 0x80) {
        # log("Error: %s", _get_modbus_exception($response_bytes[1]));
        # return;
    # }

    # Check quantity of bytes and data length
    my $bytes_cnt = $response_bytes[1];
    {
        use bytes;
        my $data_len = (length $response) - 2;
        if ($bytes_cnt != $data_len) {
            # $log->errorf("Ошибка: Длина данных не соответствует значению в поле " . 
                         # "(получено: %d, значение в поле: %d)",
                         # ((length $response) - 2), $bytes_cnt);
            return;
        }
    }

    # Return input registers
    @response_bytes[ 2 .. $#response_bytes ];
}

1;
