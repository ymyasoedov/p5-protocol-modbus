package Protocol::Modbus::Client::RTU;

use strict;
use warnings;

use Carp;
use Device::SerialPort;
use Log::Any qw($log);
use Object::Tiny;
use Protocol::Modbus::RTU::ErrorCheck qw(calc_crc check_crc);
use Time::HiRes;

sub new {
    my $class = shift;
    my $self = $class->SUPER::new(@_);

    $self->{port} = Device::SerialPort->new($self->{settings}{port}) or
        die "Error: can't open port $self->{settings}{port}: $!\n";
    
    # Configure serial port
    $self->{port}->baudrate( $self->{settings}{baudrate} );
    $self->{port}->parity  ( $self->{settings}{parity}   );
    $self->{port}->databits( $self->{settings}{databits} );
    $self->{port}->stopbits( $self->{settings}{stopbits} );
    
    $self->{port}->purge_all();
    
    return $self;
}

#
# Builds and sends request to slave and waits for response from it
#
sub query {
    my ($self, $pdu_frame, $slave_address) = @_;
    $self->_send_package($pdu_frame, $slave_address);
    
    # Receive response from slave
    my $response = $self->_receive_response();
    unless ($response) {
        $log->warn("Error: time elapsed, no response " .
                    "from slave (address: $slave_address)\n");
        return;
    }
        
    # Check modbus frame
    unless ($self->_check($response, $slave_address)) {
        return;
    }
    
    $self->_extract_pdu_frame($response);
}

sub response {
    my ($self, $pdu_frame, $address) = @_;
    $self->_send_package($pdu_frame, $address);
}

sub _send_package {
    my ($self, $pdu_frame, $address) = @_;
    my $package = $self->_build_adu_frame($address, $pdu_frame);
    # Log package
    $self->_log_package($package, type => 'request');
    $self->{port}->write($package);
}

#-------------------------------------------------------------------------------
# Private subroutines
#-------------------------------------------------------------------------------

#
# Adds CRC16 checksum to ADU frame
#
sub _add_error_checksum {
    my ($self, $frame) = @_;
    $frame . calc_crc($frame);
}

#
# Builds application data unit (ADU) frame
#
sub _build_adu_frame {
    my ($self, $address, $pdu_frame) = @_;
    $self->_add_error_checksum(pack("C", $address) . $pdu_frame);
}

#
# Returns current time in ms
#
sub _current_time {
    my $self = shift;
    my ($epoch_secs, $epoch_usecs) = Time::HiRes::gettimeofday();
    return ($epoch_secs * 1000 + int( $epoch_usecs/1000 ));
}

#
# TODO: write description
#
sub _extract_pdu_frame {
    my ($self, $adu_frame) = @_;
    use bytes;
    my $pdu_frame = substr $adu_frame, 1, -2;
    no bytes;
    $pdu_frame;
}

#
# Logs sent or received package
#
sub _log_package {
    my ($self, $package, %type) = @_;

    # Return undef if package is empty
    return unless $package;
    
    my $msg;
    foreach (unpack("C*", $package)) {
        $msg .= sprintf("[%02X]", $_);
    }
    
    if (!$type{type}) {
        $log->debug($msg);
        return;
    }
    
    if ($type{type} eq 'request') {
        $log->debug('==> ' . $msg);
    } 
    elsif ($type{type} eq 'response') {
        $log->debug('<-- ' . $msg);
    }
    else {
        $log->debug($msg);
    }
}

#
# TODO: write description
# 
sub _check {
    my ($self, $package, $expected_address) = @_;
    
    # Check checksum
    unless (check_crc($package)) {
        $log->error("Error: modbus frame has corrupted checksum\n");
        return;
    }
        
    # Check address
    if ($expected_address) {
        my $address = unpack('C', $package);
        if ($expected_address != $address) {
            $log->warn("Warning: enexpected modbus address ($address)");
            return;
        }
    }
        
    1;
}

#
# Receives response from modbus slave
#
sub _receive_response {
    my $self = shift;
    
    # TODO: remove
    my $timeout = $self->_current_time() + 3000;
    my $interval = 50;
    
    my $buf;
    my $start_package = 0;
    
    # Waiting for reply for specified timeout
    while ($self->_current_time() < $timeout) {
        Time::HiRes::usleep($interval * 1000);
    	
    	# Read up to 255 chars
        my ($count, $data) = $self->{port}->read(255);
        if ($count && $data) {
            $start_package = 1 unless $start_package;
            $buf .= $data;
        }
        else {
            last if $start_package;
        }
    }
    
    # Logging
    $self->_log_package($buf, type => 'response');

    # Return received package
    $buf;
}

sub receive_package {
    my ($self, $buf) = @_;
    
    # TODO: remove
    my $timeout = $self->_current_time() + 3000;
    my $interval = 50;
    
    # Waiting for reply for specified timeout
    while ($self->_current_time() < $timeout) {
        Time::HiRes::usleep($interval * 1000);
        # Read up to 255 chars
        my ($count, $data) = $self->{port}->read(255);
        if ($count && $data) {
            $buf .= $data;
        }
        else {
            last;
        }
    }
    
    # Logging
    $self->_log_package($buf, type => 'response');

    # Return received package
    $buf;
}

1;
