package Protocol::Modbus::RTU;
require Exporter;

use strict;
use warnings;

use Protocol::Modbus::RTU::ErrorCheck qw(build_crc16 check_crc);

our @ISA       = qw(Exporter);
our @EXPORT    = qw();
our @EXPORT_OK = qw(build_adu get_pdu_from_adu get_slave_address_from_adu);

#
# Append CRC16 to frame
#
sub append_crc16 {
    my $adu = shift;
    $adu . build_crc16($adu);
}

#
# Build application data unit (ADU) frame
#
sub build_adu {
    my ($address, $pdu) = @_;
    append_crc16(pack("C", $address) . $pdu);
}

#
#
#
sub get_pdu_from_adu {
    my $adu = shift;
    my @ba = unpack('C*', $adu);
    pack('C*', @ba[ 1 .. $#ba-2 ]);
}

#
#
#
sub get_slave_address_from_adu {
    my $adu = shift;
    unpack("C", $adu);
}

1;