package Protocol::Modbus::Server::RTU;
use strict;
use warnings;

use Carp;
use Device::SerialPort;
use Object::Tiny;
use Protocol::Modbus::Log qw(to_log);
use Protocol::Modbus::RTU qw(build_adu get_pdu_from_adu get_slave_address_from_adu);
use Time::HiRes;

#
# Modbus RTU backend constructor
#
sub new {
    my $class = shift;
    my $self = $class->SUPER::new(@_);

    $self->{port} = Device::SerialPort->new($self->{port}) or
        die "Error: can't open port $self->{settings}{port}: $!\n";

    # Configure serial port
    $self->{port}->baudrate( $self->{baudrate} );
    $self->{port}->parity  ( $self->{parity}   );
    $self->{port}->databits( $self->{databits} );
    $self->{port}->stopbits( $self->{stopbits} );

    $self->{port}->purge_all();
    return $self;
}

#
# Main loop
#
sub run {
    my $self = shift;
    to_log('info', "Running modbus server...");

    my ($buffer, $start_package);
    my $idle_timeout = 100;
    while (1) {
        my ($count, $data) = $self->{port}->read(255);
        if ($count && $data) {
            $start_package = 1;
            $buffer = $self->_receive_package($data);
        }

        if ($start_package) {
            $self->_process_request($buffer);
            $start_package = 0;
        }

        # Wait for 50 msec
        Time::HiRes::usleep(50 * 1000);
    }
}

#
# Receive package
#
sub _receive_package {
    my ($self, $buf) = @_;
    
    # TODO: remove
    my $timeout = $self->_current_time() + 3000;
    my $interval = 50;
    
    # Waiting for reply for specified timeout
    while ($self->_current_time() < $timeout) {
        Time::HiRes::usleep($interval * 1000);

        # Read up to 255 chars
        my ($count, $data) = $self->{port}->read(255);
        if ($count && $data) {
            $buf .= $data;
        }
        else {
            last;
        }
    }
    
    # Logging
    to_log('request', $buf);

    # Return received package
    $buf;
}

#
# Get current time
#
sub _current_time {
    my $self = shift;
    my ($epoch_secs, $epoch_usecs) = Time::HiRes::gettimeofday();
    return ($epoch_secs * 1000 + int( $epoch_usecs/1000 ));
}

#
# Todo: write a comment
#
sub _process_request {
    my ($self, $request) = @_;

    # Check slave address
    # my @bytes = unpack('C*', $msg);
    # my $address = $bytes[0];
    # if ($address != 0 and $address != $self->{settings}{server_address}) {
        # $log->warn("Warning: got modbus package for other slave address ($address)");
        # return;
    # }
    
    $self->{requested_slave_address} = get_slave_address_from_adu($request);
    $self->{server}->process_request(get_pdu_from_adu($request));
}

#
# Respond to master
#
sub respond {
    my $self = shift;
    my $resp = build_adu($self->{requested_slave_address}, @_);
    $self->{port}->write($resp);
    to_log('response', $resp);
}

#
# Send package to master
#
sub _send_package {
    my ($self, $pdu_frame, $address) = @_;
    my $package = $self->_build_adu_frame($address, $pdu_frame);
    to_log('response', $package);
    $self->{port}->write($package);
}

1;
