package Protocol::Modbus::Server::TCP;
use strict;
use warnings;

use Carp;
use IO::Socket;
use Object::Tiny;
use Protocol::Modbus::Log qw(to_log);
use Protocol::Modbus::TCP qw(build_adu parse_adu);

use POSIX qw(:sys_wait_h);

#
# Backend constructor
#
sub new {
    my $class = shift;
    my $self = $class->SUPER::new(@_);

    unless ($self->{port}) {
        croak "Error: port must be provided\n";
    }
    
    return $self;
}

sub REAPER {
    1 until (-1 == waitpid(-1, WNOHANG));
    $SIG{CHLD} = \&REAPER;
}

#
# Backend loop
#
sub run {
    my $self = shift;
    
    $self->{socket} = IO::Socket::INET->new(
        LocalPort => 1502,
        Type      => SOCK_STREAM,
        Reuse     => 1,
        Listen    => 10
    );
    
    my $socket = $self->{socket};
    #$socket->autoflush();
    
    my $pid;
    
    $SIG{CHLD} = \&REAPER;
    
    while (my $client = $socket->accept()) {
        
        # Forking here
        next if $pid = fork;
        die "Can't fork process: $!\n" unless defined $pid;
        my $req;
        
        while ($req = <$client>) {
            #shutdown($client, 0); # reading finished
            chomp $req;
            to_log('request', $req);
            
            my ($mbap, $pdu) = parse_adu($req); 
            my $resp = $self->{server}->process_request($pdu);
            $resp = build_adu(0, 1, $resp) . '\n';
            to_log('request', $resp);
                 
            print $client $resp;
            #shutdown($client, 1); # writing finished
        }
        
        warn "Exiting";
        exit;
    }
    
    close($self->{socket});
}

#
#
#
sub respond {
    
}

1;