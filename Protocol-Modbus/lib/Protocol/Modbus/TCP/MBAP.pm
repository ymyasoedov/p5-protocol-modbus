package Protocol::Modbus::TCP::MBAP;
require Exporter;

use strict;
use warnings;

use Carp;

our @ISA       = qw(Exporter);
our @EXPORT    = qw(build_mbap_header parse_mbap_header);
our @EXPORT_OK = qw();

#
#
#
sub build_mbap_header {
    my ($transaction_id, $length, $unit_id) = @_;
    pack("nnnC", $transaction_id, 0, $length, $unit_id);
}

#
#
#
sub parse_mbap_header {
    my $mbap_header = shift;
}

1;