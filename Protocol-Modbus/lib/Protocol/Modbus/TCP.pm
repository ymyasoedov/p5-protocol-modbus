package Protocol::Modbus::TCP;
require Exporter;

use strict;
use warnings;

use Protocol::Modbus::TCP::MBAP;

our @ISA       = qw(Exporter);
our @EXPORT    = qw();
our @EXPORT_OK = qw(build_adu parse_adu build_mbap_header parse_mbap_header);

#
# Todo: write a description
#
sub build_adu {
    my ($transact_id, $remote, $pdu) = @_;
    build_mbap_header(1, bytes::length($pdu) + 1, 0xff) . $pdu;
}

#
# Todo: write a description
#
sub parse_adu {
    my $adu = shift;
    my @ba = unpack('C*', $adu);
    return ( pack( 'C*', @ba[0 .. 6] ), pack( 'C*', @ba[7 .. $#ba] ) );
}

1;