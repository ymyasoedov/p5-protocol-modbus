package Protocol::Modbus::ExceptionCodes;
require Exporter;

use strict;
use warnings;
use Carp;

our @ISA       = qw(Exporter);
our @EXPORT    = qw();
our @EXPORT_OK = qw(%EXCEPTION_CODE);

our %EXCEPTION_CODE => (
    0x01 => 'Illegal function',
    0x02 => 'Illegal data address',
    0x03 => 'Illegal data value',
    0x04 => 'Slave device failure',
    0x05 => 'Acknowledge',
    0x06 => 'Slave device busy',
    0x08 => 'Memory parity error',
    0x0A => 'Gateway path unavailable',
    0x0B => 'Gateway target device failed to respond'
);

sub add_exception_code {
    my ($code, $exception_msg) = @_;
    if (exists $EXCEPTION_CODE{$code}) {
        carp sprintf "Standart exception message (%s (code: %X)) " .
                     "was replaced by custom message\n";
    }
    $EXCEPTION_CODE{$code} = $exception_msg;
}

sub get_exception_message {
    my $code = shift;
    if (not exists $EXCEPTION_CODE{$code}) {
        return "Unknown exception code";
    }
    return $EXCEPTION_CODE{$code};
}

1;