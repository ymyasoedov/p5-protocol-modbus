package Protocol::Modbus::Log;
require Exporter;

use strict;
use warnings;

use Log::Any qw($log);

our @ISA       = qw(Exporter);
our @EXPORT    = qw(to_log);
our @EXPORT_OK = qw(to_log);

#-------------------------------------------------------------------------------
# Simple log subroutine
#-------------------------------------------------------------------------------
sub to_log {
    my ($level, $msg) = @_;
    
    #
    # Log message
    #
    if ($level eq 'debug') {
        $log->debug($msg);
    }
    elsif ($level eq 'info') {
        $log->info($msg);
    }
    elsif ($level eq 'warn') {
        $log->warn($msg);
    }
    
    #
    # Log request or response
    #
    if ($level eq 'request' or $level eq 'response') {
        if (!$msg) {
            $log->info('no data');
            return;
        }
        my @msg_bytes = map { sprintf("[%02X]", $_) } unpack("C*", $msg);
        if ($level eq 'request') {
            $log->info('==> ' . join '', @msg_bytes);
        }
        else {
            $log->info('<-- ' . join '', @msg_bytes);
        }
    }
}

1;