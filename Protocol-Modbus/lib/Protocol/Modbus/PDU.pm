package Protocol::Modbus::PDU;
require Exporter;

use strict;
use warnings;

use Carp;
use Scalar::Util qw(looks_like_number);

our @ISA       = qw(Exporter);
our @EXPORT    = qw();
our @EXPORT_OK = qw(build_pdu_frame parse_pdu_frame);

#
# Build PDU frame
#
sub build_pdu_frame {
    my ($function_code, $data) = @_;
    
    # Check number of arguments
    unless (defined $function_code) {
        croak 'Usage: build_pdu_frame($function_code, $data = undef)';
    }
    
    # Check if function code doesn't look like number
    unless (looks_like_number $function_code) {
        croak 'Function code $function_code doesn\'t look like number';
    }
    
    # Check value of function code
    if ($function_code < 1 || $function_code > 255) {
        croak 'Modbus function must be in range from 1 to 255';
    }
    
    # Data field may be nonexistent
    if (defined $data) {
        return pack('C', $function_code) . $data;
    } 
    else {
        return pack('C', $function_code);
    }
}

#
# Parse PDU frame
#
sub parse_pdu_frame {
    my $pdu_frame = shift;
    
    # Check argument
    unless (defined $pdu_frame) {
        croak "Undefined PDU frame";
    }
    
    use bytes;
    my $pdu_frame_length = length $pdu_frame;
    no bytes;

    # Check validity of frame length
    if (!$pdu_frame_length) {
        carp "Can't parse PDU frame: frame has zero length";
        return;
    }

    # Extract function code
    my $function_code = unpack('C', $pdu_frame);

    # Check if function code doesn't look like number
    unless (looks_like_number $function_code) {
        carp "Function code $function_code doesn't look like number";
        return;
    }
    
    # Check value of function code
    if ($function_code < 1 || $function_code > 255) {
        carp "Modbus function must be in range from 1 to 255";
        return;
    }

    # Return function code and data frame, if it exists
    if ($pdu_frame_length == 1) {
        return ($function_code);
    }
    else {
        my $data = substr $pdu_frame, 1;
        return ($function_code, $data);
    }
}

1;
