package Protocol::Modbus::Client;
use strict;
use warnings;

use Carp;
use Log::Any qw($log);
use Object::Tiny;

use Protocol::Modbus::Client::TCP;

our %FUNCTIONS = (
    read_input_registers     => __PACKAGE__ . '::ReadInputRegisters',
    write_multiple_registers => __PACKAGE__ . '::WriteMultipleRegisters'
);

our %BACKEND = (
    rtu => __PACKAGE__ . '::RTU',
    tcp => __PACKAGE__ . '::TCP'
);

sub AUTOLOAD {
    my $self = shift;
    my $program = our $AUTOLOAD;
    $program =~ s/.*:://;
    return unless $program =~ /[^A-Z]/; # skip DESTROY and all-cap methods

    unless (exists $FUNCTIONS{$program}) {
        $log->warning("Warning: function $program isn't supported");
        return;
    }

    # Load module
    my $module = $FUNCTIONS{$program};
    eval "require $module; $module->import()";
    if ($@) {
	croak "Error while loading module $module: $@";
    }
    my $sub_name = $module . '::' . $program;
    if ($self->can($sub_name)) {
        $self->$sub_name(@_);
    }
    else {
        $log->error("Error: can't invoke method \"$sub_name\"");
        return;
    }
}

#
# Modbus client constructor
#
sub new {
    my $class = shift;
    my $self = $class->SUPER::new(@_);

    # Backend must be provided
    if (!$self->{backend}) {
        croak "Error: undefined backend. Use \"rtu\" or \"tcp\" backend\n";
    }

    # Setup client backend
    my $backend = lc $self->{backend};
    if (exists $BACKEND{$backend}) {
        my $backend_module = $BACKEND{$backend};
        eval "require $backend_module; $backend_module->import()";
        $self->{backend} =  $backend_module->new(
            settings => $self->{settings}
        );
    }
    else {
        croak "Error: unknown backend. Use \"rtu\" or \"tcp\" backend\n";
    }
    
    return $self;
}

#
# Add user-defined function
#
sub add_udf_function {
    my ($self, $function_name, $handler) = @_;
    $FUNCTIONS{$function_name} = $handler;
}

#
#
#
sub raw_request {
    my $self = shift;
    $self->{backend}->request(@_);
}

#
#
#
sub request {
    my $self = shift;
    $self->{backend}->request(@_);
}

1;
