package Protocol::Modbus::Server;
use strict;
use warnings;

use Carp;
use Object::Tiny;
use List::MoreUtils qw(any);
use Protocol::Modbus qw(build_pdu_frame parse_pdu_frame);
use Protocol::Modbus::Log qw(to_log);
use Scalar::Util qw(looks_like_number reftype);

our %BACKEND = (
    rtu => __PACKAGE__ . '::RTU',
    tcp => __PACKAGE__ . '::TCP'
);

our %FUNCTIONS = (
    0x03 => \&_read_holding_registers,
    0x04 => \&_read_input_registers,
    0x10 => \&_write_holding_registers,
);

#
# Modbus server constructor
#
sub new {
    my $class = shift;
    my $self = $class->SUPER::new(@_);

    # Backend must be provided
    if (!$self->{backend}) {
        croak "Error: undefined backend. Use \"rtu\" or \"tcp\" backend\n";
    }

    # Setup server backend
    my $backend = lc $self->{backend};
    if (exists $BACKEND{$backend}) {
        my $backend_module = $BACKEND{$backend};
        eval "require $backend_module; $backend_module->import()";
        if ($@) {
            croak "Error while loading $backend_module: $@";
        }
        $self->{backend} =  $backend_module->new(
            server => $self,
            @_
        );
    }
    else {
        croak "Error: unknown backend. Use \"rtu\" or \"tcp\" backend\n";
    }

    $self->init_data();

    # By default run in server mode
    $self->{mode} ||= "server";
    
    return $self;
}

#
# Main server loop
#
sub run {
    my $self = shift;
    while (1) {
        $self->{backend}->run(@_);
    }
}

#
# Add new function
#
sub add_new_function {
    my $self = shift;
    my ($function_code, $code_ref) = @_;
    
    #
    # Validate subroutine
    #
    my $usage = 'Usage: add_new_function($function_code, $code_ref)';
    if ((@_ != 2) or (any {!defined $_} @_)) {
        croak $usage . "\n" . 'Required 2 arguments';
    }
    
    if (not looks_like_number($function_code)) {
        croak $usage . "\n" . '$function_code must be a number';
    }
    
    if (reftype($code_ref) ne 'CODE') {
        croak $usage . "\n" . '$code_ref must be a ref to subroutine';
    }
    
    # From MODBUS Application Protocol Specification V1.1b3 page 10
    # MODBUS Function Code Categories
    if ($function_code < 1 or $function_code > 127) {
        croak $usage . "\n" . '$function_code must be in range [1, 127]';
    }
    
    unless (( $function_code >=  65 and $function_code <=  72 ) or
            ( $function_code >= 100 and $function_code <= 100 )) 
    {
        to_log('warn', "Function code added in public codes range");
    }
    #
    # End of subroutine validation
    #

    if ($FUNCTIONS{$function_code}) {
        to_log('warn', "Redefinition of function with code \"$function_code\"");
    }
    
    $FUNCTIONS{$function_code} = $code_ref;
    1;
}

#
#
#
sub add_route {
    my ($self, $remote, $client) = @_;
    $self->{routing_map}{$remote} = $client;
}

#
# Init data
#
sub init_data {
    my ($self, $size_ref) = @_;
    foreach (qw(holdings inputs coils discretes)) {
        if ($size_ref->{$_}) {
            $#{$self->{data}{$_}} = $size_ref->{$_};
        }
        else {
            $self->{data}{$_} = [];
        }
    }
}

#
# Process request
#
sub process_request {
    my ($self, $req) = @_;
    
    my $resp;
    
    # If server works not in gateway mode
    if ($self->{mode} ne 'gateway') {
        my ($function_code, $data) = parse_pdu_frame($req);
        
        if (exists $FUNCTIONS{$function_code}) {
            my $response_data = &{ $FUNCTIONS{$function_code} }($self, $data);
            $resp = build_pdu_frame($function_code, $response_data);
        }
        else {
            to_log('warn',
                   sprintf("Function with code %d isn't supported",
                           $function_code));
            return build_pdu_frame($function_code + 0x80, pack('C', 1));   
        }
    }
    else {
        $resp = $self->route_request($self->{backend}{requested_slave_address}, $req);
    }
    
    #$self->respond($resp);
    $resp;
}

#
#
#
sub respond {
    my $self = shift;
    $self->{backend}->respond(@_);
}

#
#
#
sub route_request {
    my ($self, $remote, $req) = @_;
    
    to_log('request', $req);
    to_log('info', 'Got request for slave with address ' . $remote);
    
    if (!$self->{routing_map}{$remote}) {
        to_log('warn', "Can't find $remote address in routing map");
        return;
    }
    
    $self->{routing_map}{$remote}->raw_request($remote, $req);
}


















#-------------------------------------------------------------------------------
#
#                          Modbus Functions
#
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Modbus function "read input registers"
#-------------------------------------------------------------------------------
sub _read_input_registers {
    my ($self, $data) = @_;
    my ($starting_address, $quantity_of_registers) = unpack('nn', $data);
    pack('Cn*', $quantity_of_registers * 2, ( 1 .. $quantity_of_registers ));
}

#-------------------------------------------------------------------------------
# Modbus function "read holding registers"
#-------------------------------------------------------------------------------
sub _read_holding_registers {
    my ($self, $data) = @_;
    my ($starting_address, $quantity_of_registers) = unpack('nn', $data);
    my $ending_address = $starting_address + $quantity_of_registers - 1;  
    my @values = map( ${$self->{data}{holdings}}[$_], $starting_address .. $ending_address );
    pack('Cn*', $quantity_of_registers * 2, ( 1 .. $quantity_of_registers ));
}

#-------------------------------------------------------------------------------
# Modbus function "write holding registers"
#-------------------------------------------------------------------------------
sub _write_holding_registers {
    my ($self, $data) = @_;
    my ($starting_address,
        $quantity_of_registers,
        $byte_count,
        @values) = unpack('nnCn*', $data);
    for (my $i = 0; $i < $quantity_of_registers; $i++) {
        ${$self->{data}{holdings}}[$starting_address + $i] = $values[$i];
    }
    pack('nn', $starting_address, $quantity_of_registers);
}

1;