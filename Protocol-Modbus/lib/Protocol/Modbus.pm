package Protocol::Modbus;
require Exporter;

use strict;
use warnings;

use Carp;
use Protocol::Modbus::PDU qw(build_pdu_frame parse_pdu_frame);

our @ISA       = qw(Exporter);
our @EXPORT    = qw();
our @EXPORT_OK = qw(%FUNCTION_CODE build_pdu_frame parse_pdu_frame);

our $VERSION = '0.200001';

our %FUNCTION_CODE = (
    ############################################################################
    # Data Access
    ############################################################################
    
    ############################################################################
    #### Bit access
    ############################################################################
    
    #### Physical Discrete Inputs
    
    read_discrete_inputs             => 0x02,
    
    #### Internal Bits or Physical Coils
    
    read_coils                       => 0x01,
    write_single_coil                => 0x05,
    write_multiple_coils             => 0x0F,
    
    ############################################################################
    #### 16 bits access
    ############################################################################
    
    #### Physical Input Registers 
    
    read_input_registers             => 0x04,
    
    #### Internal Registers or Physical Output Registers
    
    read_holding_registers           => 0x03,
    write_single_register            => 0x06,
    write_multiple_registers         => 0x10,
    read_write_multiple_registers    => 0x17,
    mask_write_registers             => 0x16,
    read_fifo_queue                  => 0x18,
    
    ############################################################################
    #### File record access
    ############################################################################
    
    read_file_record                 => 0x14,
    write_file_record                => 0x15,
    
    ############################################################################
    # Diagnostics
    ############################################################################
    
    read_exception_status            => 0x07,
    diagnostic                       => 0x08,
    get_com_event_counter            => 0x0B,
    get_com_event_log                => 0x0C,
    report_server_id                 => 0x11,
    read_device_identification       => 0x2B,
    
    ############################################################################
    # Other
    ############################################################################
    
    encapsulated_interface_transport => 0x2B,
    canopen_general_interface        => 0x2B
);

1;
