#!/usr/bin/perl
use strict;
use warnings;

use Protocol::Modbus::Server;

use Try::Tiny;
use Test::More tests => 16;

# Test
try {
    my $server = Protocol::Modbus::Server->new();
    ok(!$server, "Protocol::Modbus::Server->new()");
} catch {
    ok($_ =~ /^Error\: undefined backend/,
       "Protocol::Modbus::Server->new()"
    );
};

# Test
try {
    my $server = Protocol::Modbus::Server->new(backend => '');
    ok(!$server, "Protocol::Modbus::Server->new()");
} catch {
    ok($_ =~ /^Error\: undefined backend/,
       "Protocol::Modbus::Server->new(backend => '')"
    );
};

# Test
try {
    my $server = Protocol::Modbus::Server->new(backend => 'invalid');
    ok(!$server, "Protocol::Modbus::Server->new()");
} catch {
    ok($_ =~ /^Error\: unknown backend/,
       "Protocol::Modbus::Server->new(backend => 'invalid')"
    );
};

# Test
try {
    my $server = Protocol::Modbus::Server->new(backend => 'tcp');
} catch {
    ok($_ =~ /^Error\: port must be provided/,
       "Protocol::Modbus::Server->new(backend => 'tcp')"
    );
};

# Test
try {
    my $server = Protocol::Modbus::Server->new(backend => 'tcp', port => 502);
    ok(defined($server) && ref $server eq 'Protocol::Modbus::Server',
       "Protocol::Modbus::Server->new(backend => 'tcp', port => 502)"
    );
} catch {
    ok(0, "Protocol::Modbus::Server->new(backend => 'tcp', port => 502)");
};

# Test
try {
    my $server = Protocol::Modbus::Server->new(backend => 'tcp', port => 502);
    is_deeply($server->{data}, 
              { holdings => [], inputs => [], coils => [], discretes => [] },
              "Init default data"
    );
} catch {
    fail("Init default data");
};

# Test
try {
    my $server = Protocol::Modbus::Server->new(backend => 'tcp', port => 502);
    $server->add_new_function();
} catch {
    ok($_ =~ /Required 2 arguments/,
       "Protocol::Modbus::Server::add_new_function()");
};

# Test
try {
    my $server = Protocol::Modbus::Server->new(backend => 'tcp', port => 502);
    $server->add_new_function(undef);
} catch {
    ok($_ =~ /Required 2 arguments/,
       "Protocol::Modbus::Server::add_new_function(undef)");
};

# Test
try {
    my $server = Protocol::Modbus::Server->new(backend => 'tcp', port => 502);
    $server->add_new_function(undef, undef, undef);
} catch {
    ok($_ =~ /Required 2 arguments/,
       "Protocol::Modbus::Server::add_new_function(undef, undef, undef)");
};

# Test
try {
    my $server = Protocol::Modbus::Server->new(backend => 'tcp', port => 502);
    my $test_cb = sub { };
    $server->add_new_function(0, $test_cb);
} catch {
    ok($_ =~ /must be in range/,
       'Protocol::Modbus::Server::add_new_function(0, $test_cb)'
    );
};

# Test
try {
    my $server = Protocol::Modbus::Server->new(backend => 'tcp', port => 502);
    my $test_cb = sub { };
    $server->add_new_function(128, $test_cb);
} catch {
    ok($_ =~ /must be in range/,
       'Protocol::Modbus::Server::add_new_function(128, $test_cb)'
    );
};

# Test
try {
    my $server = Protocol::Modbus::Server->new(backend => 'tcp', port => 502);
    $server->add_new_function(1, undef);
} catch {
    ok($_ =~ /Required 2 arguments/,
       'Protocol::Modbus::Server::add_new_function(1, undef)'
    );
};

# Test
try {
    my $server = Protocol::Modbus::Server->new(backend => 'tcp', port => 502);
    my $test_cb = sub { };
    $server->add_new_function(1, $test_cb);
    ok(1, 'Protocol::Modbus::Server::add_new_function(1, $test_cb)');
} catch {
    ok(0, 'Protocol::Modbus::Server::add_new_function(1, $test_cb)');
};

# Test
try {
    my $server = Protocol::Modbus::Server->new(backend => 'tcp', port => 502);
    my $test_cb = sub { };
    $server->add_new_function(65, $test_cb);
    ok(1, 'Protocol::Modbus::Server::add_new_function(65, $test_cb)');
} catch {
    ok(0, 'Protocol::Modbus::Server::add_new_function(65, $test_cb)');
};

# Test
try {
    my $server = Protocol::Modbus::Server->new(backend => 'tcp', port => 502);
    my $test_cb = sub { };
    $server->add_new_function(127, $test_cb);
    ok(1, 'Protocol::Modbus::Server::add_new_function(127, $test_cb)');
} catch {
    ok(0, 'Protocol::Modbus::Server::add_new_function(127, $test_cb)');
};

# Test
try {
    my $server = Protocol::Modbus::Server->new(backend => 'tcp', port => 502);
    my $test_cb = sub { };
    $server->add_new_function('foo', $test_cb);
    ok(0, 'Protocol::Modbus::Server::add_new_function(\'foo\', $test_cb)');
} catch {
    ok($_ =~ /must be a number/,
       'Protocol::Modbus::Server::add_new_function(\'foo\', $test_cb)'
    );
};